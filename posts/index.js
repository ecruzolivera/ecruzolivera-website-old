export default () => {
  function importAll(r) {
    const postsTmp = {}
    r.keys().forEach(key => (postsTmp[key.slice().replace('./', '')] = r(key)))
    const keys = Object.keys(postsTmp).sort()
    const posts = keys.map(key => {
      let [id, title] = key.split('-')
      id = parseInt(id.trim())
      title = title.trim().split('.')[0]
      return { id: id, title: title, content: postsTmp[key] }
    })
    return posts
  }
  return importAll(require.context('./', false, /\.md$/))
}
