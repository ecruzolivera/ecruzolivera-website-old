const withSass = require('@zeit/next-sass')
const withCss = require('@zeit/next-css')

module.exports = withCss({
  exportPathMap: () => {
    return {
      '/': { page: '/' },
      '/cv': { page: '/cv' },
      '/blog': { page: '/blog' },
      '/blog?id=1': { page: '/blog', query: { id: '1' } },
      '/blog?id=2': { page: '/blog', query: { id: '2' } }
    }
  },
  webpack: (config, { isServer }) => {
    config.module.rules.push({
      test: /\.md$/,
      use: 'raw-loader'
    })
    return config
  }
})
