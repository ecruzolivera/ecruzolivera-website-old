import React, { Component } from 'react'
import { withRouter } from 'next/router'
import Markdown from 'react-markdown'
import _ from 'lodash'
import CodeBlock from '../components/CodeBlock'
import Layout from '../components/Layout'
import getPosts from '../posts'

class Blog extends React.Component {
  static async getInitialProps() {
    const posts = getPosts()
    return { posts }
  }
  render() {
    const { query } = this.props.router
    const posts = this.props.posts
    const links = posts.map(post => {
      return { id: post.id, title: post.title }
    })
    const lastPost = posts[posts.length - 1]
    const postToRenderId = query.id ? query.id : lastPost.id
    console.log(postToRenderId)
    const candidatePost = _.find(posts, post => post.id == postToRenderId)
    const postToRender = candidatePost ? candidatePost : lastPost
    return (
      <Layout>
        <section>
          <article>
            <Markdown
              source={postToRender.content}
              renderers={{ code: CodeBlock }}
              className='container'
            />
          </article>
        </section>
      </Layout>
    )
  }
}
export default withRouter(Blog)
