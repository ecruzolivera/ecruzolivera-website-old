import Layout from '../components/Layout'
import PlaceCard from '../components/PlaceCard'

export default () => {
  const data = {
    education: [
      {
        start_date: '2017',
        end_date: '2018',
        title: 'Master in Electronic Systems Engineering',
        place: 'UC3M Campus Leganés',
        city: 'Madrid',
        country: 'Spain'
      },
      {
        start_date: '2006',
        end_date: '2011',
        title: 'Engineering in Telecommunications and Electronics',
        place: 'Technological University of Havana ',
        city: 'Havana',
        country: 'Cuba'
      },
      {
        start_date: '2001',
        end_date: '2005',
        title: 'Bachelor of Science',
        place: 'I.P.V.C. Vladimir I. Lenin',
        city: 'Havana',
        country: 'Cuba'
      }
    ],
    jobs: [
      {
        start_date: '2011',
        end_date: '2017',
        title: 'Embedded System Specialist',
        place: 'Cuban Neuroscience Center',
        city: 'Havana',
        country: 'Cuba'
      }
    ]
  }
  return (
    <Layout>
      <section>
        <article className='container'>
          <div className='education'>
            <h1>Education</h1>
            <ul>
              {data.education.map((school, index) => {
                return (
                  <li key={index}>
                    <hr />
                    <PlaceCard place={school} index={index} />
                  </li>
                )
              })}
            </ul>
          </div>
        </article>
        <article className='container'>
          <div className='jobs'>
            <h1>Job Experience</h1>
            <ul>
              {data.jobs.map((job, index) => {
                return (
                  <li key={index}>
                    <hr />
                    <PlaceCard place={job} index={index + 1} />
                  </li>
                )
              })}
            </ul>
          </div>
        </article>
      </section>
      <style jsx>{`
        h1 {
          text-align: center;
          padding-left: 10%;
        }
        li {
          list-style-type: none;
        }
        hr {
          width: 100%;
          margin: 1em;
          text-align: center;
        }
        @media only screen and (min-width: 768px) {
          h1 {
            text-align: left;
            padding-left: 0%;
          }
          hr {
            width: 75%;
            margin: 1em;
            margin-left: 0;
          }
        }
      `}</style>
    </Layout>
  )
}
