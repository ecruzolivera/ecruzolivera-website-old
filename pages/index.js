import Layout from '../components/Layout'

export default () => (
  <Layout>
    <section>
      <article className='container'>
        <img
          src='/static/me.jpg'
          alt='Ernesto pic'
          height='200px'
          width='200px'
        />
        <div>
          <p>
            <strong>Hi</strong>, i'm Ernesto Cruz Olivera.
          </p>
          <p>An embedded systems engineer by trade.</p>
          <p>Who likes tinkering with microcontrollers.</p>
          <p>
            Finding{' '}
            <a href='https://reactjs.org/' target='_blank'>
              React
            </a>
            ,{' '}
            <a
              href='https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout'
              target='_blank'
            >
              CSS Grid
            </a>{' '}
            and{' '}
            <a
              href='https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox'
              target='_blank'
            >
              FlexBox
            </a>{' '}
            fascinating.
          </p>
          <p>Very convinced that the web technologies are the future.</p>
          <p>
            I'm <strong>always</strong> learning.
          </p>
        </div>
      </article>
    </section>
    <style jsx>{`
      article {
        display: flex;
        align-content: center;
        justify-content: center;
        align-content: center;
        align-items: center;
        flex-wrap: wrap;
      }
      article img {
        margin: 0 1em;
        border-radius: 0.5em;
      }
      p {
        text-align: center;
      }
      @media only screen and (min-width: 768px) {
        p {
          text-align: left;
        }
      }
    `}</style>
  </Layout>
)
