import Head from 'next/head'
import Header from './Header'
import Footer from './Footer'

export default props => (
  <div>
    <Head>
      <title>ecruzolivera</title>
      <meta name='viewport' content='initial-scale=1.0, width=device-width' />
      <link
        rel='stylesheet'
        href='https://cdnjs.cloudflare.com/ajax/libs/modern-normalize/0.5.0/modern-normalize.min.css'
        integrity='sha256-N6+kUxTWxpqVK+BrPWt3t4jeOWPtp37RZEbm5n9X+8U='
        crossOrigin='anonymous'
      />
      <link
        rel='stylesheet'
        href='https://use.fontawesome.com/releases/v5.6.3/css/all.css'
        integrity='sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/'
        crossOrigin='anonymous'
      />
    </Head>
    <div className='pseudo-body'>
      <Header />
      <main>{props.children}</main>
      <Footer />
    </div>
    <style jsx global>{`
      p {
        margin-bottom: 1em;
      }
      a {
        text-decoration: none;
      }
      a:hover {
        opacity: 0.8;
      }
      p a {
        text-decoration: underline;
        background-color: rgba(187, 239, 253, 0.3);
      }
      h1,
      h2,
      strong,
      a {
        color: #333;
      }

      body {
        font-family: sans-serif;
        line-height: 1.5;
        color: #202121;
        background-color: #f4f7f6;
      }
      .pseudo-body {
        display: grid;
        grid-template-areas:
          'header'
          'main'
          'footer';
        grid-template-columns: auto;
        grid-template-rows: auto 1fr auto auto;
        grid-gap: 10px;
        height: 100vh;
      }
      header {
        grid-area: header;
      }
      main {
        grid-area: main;
      }
      footer {
        grid-area: footer;
      }
      aside {
        grid-area: aside;
      }
      .container {
        max-width: 50em;
        margin: 0 auto;
        padding: 1em;
      }
      .inverted,
      .inverted a {
        background-color: #2b2d32;
        color: whitesmoke;
      }
      .text_align_right {
        text-align: right;
      }
    `}</style>
  </div>
)
