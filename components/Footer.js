export default () => {
  const now = new Date(Date.now())
  return (
    <footer className='footer'>
      <p>{now.getFullYear()}</p>
      <style jsx>{`
        footer {
          text-align: center;
        }
      `}</style>
    </footer>
  )
}
