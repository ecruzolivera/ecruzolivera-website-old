export default props => {
  const { title, place, city, country } = props
  return (
    <div>
      <h2>{title}</h2>
      <p>{place}</p>
      <p className='italic'>
        {city}, {country}.
      </p>
      <style jsx>{`
        h2,
        p {
          text-align: center;
          font-size: 0.9em;
          margin: 0;
          vertical-align: baseline;
        }
        .italic {
          font-style: italic;
        }
        @media only screen and (min-width: 768px) {
          h2,
          p {
            text-align: left;
          }
        }
      `}</style>
    </div>
  )
}
