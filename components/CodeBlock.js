import React from 'react'
import PropTypes from 'prop-types'
import hljs from 'highlight.js'
import 'highlight.js/styles/default.css'

export default class CodeBlock extends React.PureComponent {
  constructor(props) {
    super(props)
    this.setRef = this.setRef.bind(this)
  }

  setRef(el) {
    this.codeEl = el
  }

  componentDidMount() {
    this.highlightCode()
  }

  componentDidUpdate() {
    this.highlightCode()
  }

  highlightCode() {
    hljs.highlightBlock(this.codeEl)
  }

  render() {
    return (
      <pre>
        <p ref={this.setRef} className={`language-${this.props.language}`}>
          {this.props.value}
        </p>
        <style jsx>{`
          pre {
            white-space: pre-wrap;
          }
        `}</style>
      </pre>
    )
  }
}

CodeBlock.defaultProps = {
  language: ''
}

CodeBlock.propTypes = {
  value: PropTypes.string.isRequired,
  language: PropTypes.string
}
