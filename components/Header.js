import Link from 'next/link'
export default () => {
  const blogLinks = [
    {
      label: 'Cpp for Embedded Systems Part 1: Heapless Containers',
      url: '/blog?id=1'
    },
    {
      label: 'Cpp for Embedded Systems Part 2: Delegates and Signals',
      url: '/blog?id=2'
    }
  ]
  return (
    <header className='inverted'>
      <nav role='navigation' aria-label='main navigation'>
        <div className='nav_links'>
          <div key='Me'>
            <Link href='/'>
              <a>Me</a>
            </Link>
          </div>
          <div key='Blog' className='dropdown'>
            <Link href={{ pathname: '/blog', query: { name: 'id' } }}>
              <a>Blog</a>
            </Link>
            <div className='dropdown-content'>
              {blogLinks.map(link => {
                return (
                  <div key={link.label}>
                    <Link href={link.url}>
                      <a target='_blank'>{link.label}</a>
                    </Link>
                  </div>
                )
              })}
            </div>
          </div>
          <div key='CV'>
            <Link href='/cv'>
              <a>CV</a>
            </Link>
          </div>
        </div>
        <div className='nav_icons'>
          <a href='https://gitlab.com/ecruzolivera' target='_blank'>
            <i className='fab fa-gitlab' />
          </a>
          <a
            href='https://www.linkedin.com/in/ernesto-cruz-olivera/'
            target='_blank'
          >
            <i className='fab fa-linkedin' />
          </a>
          <a
            href='mailto:ecruzolivera@gmail.com?subject=Reaching%20Out&body=How%20are%20you'
            target='_blank'
          >
            <i className='fas fa-envelope' />
          </a>
        </div>
      </nav>
      <style jsx>{`
        a {
          display: block;
          padding: 1em;
          font-weight: bold;
        }
        nav {
          display: flex;
          flex-direction: column;
          align-content: center;
          justify-content: top;
          align-items: center;
        }
        .nav_links {
          display: flex;
          align-content: center;
          justify-content: center;
          text-align: center;
          order: 1;
        }
        .nav_icons {
          display: flex;
          align-content: center;
          justify-content: center;
          text-align: center;
        }
        .dropdown {
          position: relative;
        }
        .dropdown-content {
          display: none;
          position: absolute;
          min-width: 300px;
          text-align: left;
        }

        .dropdown:hover .dropdown-content {
          display: flex;
          flex-direction: column;
          transition-delay: 1s;
        }

        i {
          font-size: 1.2em;
          font-weight: bold;
        }

        @media only screen and (min-width: 768px) {
          nav {
            margin: 0 25% 0 25%;
            flex-direction: row;
            align-content: center;
            justify-content: space-between;
          }
          .nav_links {
            justify-content: flex-start;
            order: 1;
          }
          .nav_icons {
            justify-content: flex-end;
            order: 2;
          }
        }
      `}</style>
    </header>
  )
}
