export default props => {
  const { start_date, end_date } = props
  return (
    <p>
      {start_date}
      {end_date ? ' - ' + end_date : ''}
      <style jsx>{`
        p {
          text-align: center;
          font-size: 0.9em;
          margin: 0;
          vertical-align: text-bottom;
        }
        @media only screen and (min-width: 768px) {
          p {
            text-align: right;
            margin-right: 1em;
          }
        }
      `}</style>
    </p>
  )
}
