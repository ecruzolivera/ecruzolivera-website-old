import DateBox from './DateBox'
import PlaceBox from './PlaceBox'

export default props => {
  const { place } = props
  return (
    <div>
      <DateBox start_date={place.start_date} end_date={place.end_date} />
      <PlaceBox
        title={place.title}
        place={place.place}
        city={place.city}
        country={place.country}
      />
      <style jsx>{`
        div {
          display: flex;
          align-content: center;
          justify-content: center;
          flex-direction: column;
        }

        @media only screen and (min-width: 768px) {
          div {
            justify-content: flex-start;
            flex-direction: row;
          }
        }
      `}</style>
    </div>
  )
}
