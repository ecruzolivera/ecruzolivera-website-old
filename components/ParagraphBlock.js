import React from 'react'
import PropTypes from 'prop-types'

export default class ParagraphBlock extends React.PureComponent {
  constructor(props) {
    super(props)
    this.setRef = this.setRef.bind(this)
  }
  setRef(el) {
    this.codeEl = el
  }
  render() {
    console.log(this.props)
    return (
      <p ref={this.setRef}>
        {this.props.value}
        <style jsx>{`
          p {
            text-align: justify;
          }
        `}</style>
      </p>
    )
  }
}
ParagraphBlock.propTypes = {
  value: PropTypes.string.isRequired
}
